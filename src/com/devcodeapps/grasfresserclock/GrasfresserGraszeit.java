package com.devcodeapps.grasfresserclock;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;

public class GrasfresserGraszeit extends Activity {

	private static final String logTag = GrasfresserGraszeit.class
			.getSimpleName();
	private Map<String, Integer> timeMappingGreen;
	private Map<String, Float> brightnessMapping;
	private WindowManager.LayoutParams lp;
	private int displayOrientation;
	private int prevScreenTimeout;
	private int prevScreenBrightness;
	private Thread timeThread;
	private String prefBrightness = "50";
	private String[] dates;
	private int dateTextCount = 0;
	private int datecounter = 0;
	private boolean appStart = true;
	private ImageView imageDay;
	private Calendar cal;
	private String hour1;
	private String hour2;
	private String minute1;
	private String minute2;
	private ImageView imageHour1v;
	private ImageView imageHour2v;
	private ImageView imageMinute1v;
	private ImageView imageMinute2v;
	private ImageView imageGrasfresserLogo;
	private ImageView imageClockDots;

	private Handler timeHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			updateTimeGui();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// get stored preferences
		if (savedInstanceState != null) {
			onRestoreInstanceState(savedInstanceState);
		}

		// fetch prev system preferences
		else {
			// get prev screen brightness
			try {
				prevScreenBrightness = android.provider.Settings.System
						.getInt(getContentResolver(),
								Settings.System.SCREEN_BRIGHTNESS);
				Log.d(logTag, "onCreate : prev screen brightness saved : "
						+ prevScreenBrightness);
			} catch (SettingNotFoundException e) {
				Log.e(logTag,
						"onCreate : ERROR : cannot fetch screen brightness : "
								+ e);
			}

			// get prev screen timeout
			try {
				prevScreenTimeout = android.provider.Settings.System.getInt(
						getContentResolver(),
						Settings.System.SCREEN_OFF_TIMEOUT);
				Log.d(logTag, "onCreate : default screen timeout saved : "
						+ prevScreenTimeout);
			} catch (SettingNotFoundException e) {
				Log.e(logTag,
						"onCreate : ERROR : cannot fetch screen timeout : " + e);
			}
		}

		getPreferences();

		// temp disable date infos
		/*
		 * dates = new String[3]; try { getDates(); } catch (IOException e) {
		 * Log.d(logTag, "onCreate : ERROR : cannot fetch dates from server : "
		 * + e); e.printStackTrace(); }
		 */

		// init values
		if (appStart) {
			setPreferences();
			setBrightnessMapping();
			setBrightness();
			setTimeMappingGreen();
			initViews();
			appStart = false;
		}

		// fetch screen orientation
		displayOrientation = getResources().getConfiguration().orientation;
		Log.d(logTag, "onCreate() : displayOrientation = " + displayOrientation);

		// vertical
		if (displayOrientation == 1) {
			setContentView(R.layout.clockv);
		}
		// horizontal
		else {
			setContentView(R.layout.clockh);
		}
		updateTime();
	}

	private void initViews() {
		imageHour1v = (ImageView) findViewById(R.id.imghour1);
		imageHour2v = (ImageView) findViewById(R.id.imghour2);
		imageMinute1v = (ImageView) findViewById(R.id.imgminute1);
		imageMinute2v = (ImageView) findViewById(R.id.imgminute2);
		imageGrasfresserLogo = (ImageView) findViewById(R.id.imgGrasfresserLogo);
		imageClockDots = (ImageView) findViewById(R.id.clockdots);
	}

	private void getPreferences() {
		final SharedPreferences pref = Preferences.getAppPreferences(this);
		prefBrightness = pref.getString("prefBrightness", prefBrightness);
	}

	/**
	 * get stored app preferences and store to current SharedPreferences
	 */
	private void setPreferences() {
		final SharedPreferences pref = Preferences.getAppPreferences(this);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("prefBrightness", prefBrightness);
		editor.commit();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean("appStart", appStart);
		outState.putInt("prevScreenTimeout", prevScreenTimeout);
		Log.d(logTag,
				"onSaveInstanceState() : save default display-timeout : timeout :"
						+ prevScreenTimeout);
		outState.putInt("prevScreenBrightness", prevScreenBrightness);
		Log.d(logTag,
				"onSaveInstanceState() : save default display-brightness : brightness :"
						+ prevScreenBrightness);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		appStart = savedInstanceState.getBoolean("appStart");
		prevScreenTimeout = savedInstanceState.getInt("prevScreenTimeout");
		Log.d(logTag,
				"onSaveInstanceState() : saved default display-timeout : timeout :"
						+ prevScreenTimeout);
		prevScreenBrightness = savedInstanceState
				.getInt("prevScreenBrightness");
		Log.d(logTag,
				"onSaveInstanceState() : saved default display-brightness : brightness :"
						+ prevScreenBrightness);
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		getPreferences();
		setBrightnessMapping();
		setBrightness();
		setTimeMappingGreen();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		android.provider.Settings.System.putInt(getContentResolver(),
				Settings.System.SCREEN_OFF_TIMEOUT, prevScreenTimeout);
		Log.d(logTag,
				"onDestroy() : reset screen timeout to default : timeout :"
						+ prevScreenTimeout);
		android.provider.Settings.System.putInt(getContentResolver(),
				Settings.System.SCREEN_BRIGHTNESS, prevScreenBrightness);
		Log.d(logTag,
				"onDestroy() : reset screen brightness to default brightness :"
						+ prevScreenBrightness);
		super.onDestroy();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater mi = new MenuInflater(getApplication());
		mi.inflate(R.menu.mainmenu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.opt_settings:
			Intent preferenceActivity = new Intent(getBaseContext(),
					Preferences.class);
			startActivity(preferenceActivity);
			break;
		case R.id.opt_about:
			showAboutDialog();
			break;
		case R.id.opt_exit:
			finish();
			break;
		}
		return true;
	}

	private void showAboutDialog() {
		Builder builder = new Builder(this);
		builder.setTitle(R.string.om_main_about_header)
				.setIcon(R.drawable.icon)
				.setMessage(R.string.om_main_about_text)
				.setNeutralButton(android.R.string.ok, null).show();
	}

	public void updateTime() {
		timeThread = new Thread() {

			@Override
			public void run() {
				while (true) {
					try {
						timeHandler.sendMessage(timeHandler.obtainMessage(0));
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						Log.d(logTag, "updateTime() : ERROR update time : " + e);
						e.printStackTrace();
					}
				}
			}

		};
		timeThread.start();
	}

	public void updateTimeGui() {
		ImageView imageHour1v = (ImageView) findViewById(R.id.imghour1);
		ImageView imageHour2v = (ImageView) findViewById(R.id.imghour2);
		ImageView imageMinute1v = (ImageView) findViewById(R.id.imgminute1);
		ImageView imageMinute2v = (ImageView) findViewById(R.id.imgminute2);

		ImageView imageGrasfresserLogo = (ImageView) findViewById(R.id.imgGrasfresserLogo);
		ImageView imageClockDots = (ImageView) findViewById(R.id.clockdots);

		/*
		 * dates temp disabled TextView textViewDates = (TextView)
		 * findViewById(R.id.dateText); datecounter++; Log.d(logTag,
		 * "updateTime() : dateTextCount: " + dateTextCount); if (datecounter %
		 * 4 == 0) { if (dateTextCount >= 2) { dateTextCount = 0; } else {
		 * dateTextCount++; } } textViewDates.setText(dates[dateTextCount]);
		 */

		cal = Calendar.getInstance();

		// hours
		if (cal.getTime().getHours() < 10) {
			hour1 = "0";
			hour2 = String.valueOf(cal.getTime().getHours()).substring(0, 1);
		} else {
			hour1 = String.valueOf(cal.getTime().getHours()).substring(0, 1);
			hour2 = String.valueOf(cal.getTime().getHours()).substring(1, 2);
		}

		// minutes
		if (cal.getTime().getMinutes() < 10) {
			minute1 = "0";
			minute2 = String.valueOf(cal.getTime().getMinutes())
					.substring(0, 1);
		} else {
			minute1 = String.valueOf(cal.getTime().getMinutes())
					.substring(0, 1);
			minute2 = String.valueOf(cal.getTime().getMinutes())
					.substring(1, 2);
		}

		// get day image from layout
		switch (cal.get(Calendar.DAY_OF_WEEK)) {
		case 1:
			imageDay = (ImageView) findViewById(R.id.imgSun);
			break;
		case 2:
			imageDay = (ImageView) findViewById(R.id.imgMon);
			break;
		case 3:
			imageDay = (ImageView) findViewById(R.id.imgTue);
			break;
		case 4:
			imageDay = (ImageView) findViewById(R.id.imgWed);
			break;
		case 5:
			imageDay = (ImageView) findViewById(R.id.imgThu);
			break;
		case 6:
			imageDay = (ImageView) findViewById(R.id.imgFri);
			break;
		case 0:
			imageDay = (ImageView) findViewById(R.id.imgSat);
			break;
		}

		// set day of week
		setDayImageGreen(cal.get(Calendar.DAY_OF_WEEK));
		Log.d(logTag, "updateTimeGui() : day of week : " + Calendar.DAY_OF_WEEK);

		// set time images
		imageHour1v.setImageResource(timeMappingGreen.get(hour1));
		imageHour2v.setImageResource(timeMappingGreen.get(hour2));
		imageMinute1v.setImageResource(timeMappingGreen.get(minute1));
		imageMinute2v.setImageResource(timeMappingGreen.get(minute2));
		imageClockDots.setImageResource(0x7f02000a);
		imageGrasfresserLogo.setImageResource(0x7f02000c);
	}

	/**
	 * set stored brightness from SharedPreferences
	 */
	public void setBrightness() {
		lp = getWindow().getAttributes();
		lp.screenBrightness = brightnessMapping.get(prefBrightness);
		getWindow().setAttributes(lp);
	}

	/**
	 * mapping for green images
	 */
	public void setTimeMappingGreen() {
		timeMappingGreen = new HashMap<String, Integer>();
		timeMappingGreen.put("0", 0x7f020000);
		timeMappingGreen.put("1", 0x7f020001);
		timeMappingGreen.put("2", 0x7f020002);
		timeMappingGreen.put("3", 0x7f020003);
		timeMappingGreen.put("4", 0x7f020004);
		timeMappingGreen.put("5", 0x7f020005);
		timeMappingGreen.put("6", 0x7f020005);
		timeMappingGreen.put("7", 0x7f020007);
		timeMappingGreen.put("8", 0x7f020008);
		timeMappingGreen.put("9", 0x7f020009);
	}

	/**
	 * mapping for brightness
	 */
	public void setBrightnessMapping() {
		brightnessMapping = new HashMap<String, Float>();
		brightnessMapping.put("100", 1.0f);
		brightnessMapping.put("75", 0.75f);
		brightnessMapping.put("50", 0.5f);
		brightnessMapping.put("25", 0.25f);
		brightnessMapping.put("5", 0.05f);
	}

	/**
	 * set right image for current day 
	 * @param day
	 */
	public void setDayImageGreen(int day) {
		switch (day) {
		case 1:
			imageDay.setImageResource(0x7f020010);
			break;
		case 2:
			imageDay.setImageResource(0x7f02000e);
			break;
		case 3:
			imageDay.setImageResource(0x7f020012);
			break;
		case 4:
			imageDay.setImageResource(0x7f020013);
			break;
		case 5:
			imageDay.setImageResource(0x7f020011);
			break;
		case 6:
			imageDay.setImageResource(0x7f02000b);
			break;
		case 0:
			imageDay.setImageResource(0x7f02000f);
			break;
		}
	}
	
	
	//private void getDates() throws IOException {

		/*
		 * Variante 1 -> save content with byte-array URL dates = new URL(
		 * "http://grasfresser.bplaced.net/android/dates.txt");
		 * 
		 * InputStream isText = dates.openStream(); dateText = new byte[250];
		 * int readSize = isText.read(dateText); Log.d(logTag, "readSize = " +
		 * readSize); Log.d(logTag, "bText = "+ new String(dateText, "UTF-8"));
		 * isText.close();
		 */

		/* Variante 2 -> save content with http-classes and String */
		/*
		HttpPost request = new HttpPost(
				"http://grasfresser.bplaced.net/android/dates.txt");
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(request);

		HttpEntity entity = response.getEntity();
		// gfDateText = EntityUtils.toString(entity, "UTF-8");

		InputStream instream = entity.getContent();
		BufferedReader br = new BufferedReader(new InputStreamReader(instream,
				"UTF-8"), 8);

		String tempDates;
		String[] tempList;
		int i = 0;
		int[] list = new int[34];

		// gfDateText = br.readLine();
		while ((tempDates = br.readLine()) != null) {
			tempList = tempDates.split(";");
			dates[i] = new String();
			dates[i] = tempList[0];
			// String[] parts = gfDateText.split(";");
			// list[i] = Integer.parseInt()(parts[0]);
			i++;
		 }
		}
		*/
}