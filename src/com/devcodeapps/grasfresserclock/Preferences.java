package com.devcodeapps.grasfresserclock;

import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Preferences extends PreferenceActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		this.addPreferencesFromResource(R.xml.grasfresserclock_settings);
	}
	
	public static final SharedPreferences getAppPreferences(final ContextWrapper ctx){
		return ctx.getSharedPreferences(
				ctx.getPackageName() +
				"_preferences", MODE_PRIVATE);
	}
}
